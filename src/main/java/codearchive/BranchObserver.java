package codearchive;

public interface BranchObserver {
    void update();
}
