package codearchive;

public class Code {
    private final String content;

    public Code(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
