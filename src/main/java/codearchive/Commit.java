package codearchive;

import projectmanagement.Developer;

import java.time.LocalDateTime;

public class Commit {
    public final Developer byDeveloper;
    public final Code newCode;
    public LocalDateTime createdOn;


    public Commit(Developer byDeveloper, Code newCode) {
        this.byDeveloper = byDeveloper;
        this.newCode = newCode;
        this.createdOn = LocalDateTime.now();
    }
}
