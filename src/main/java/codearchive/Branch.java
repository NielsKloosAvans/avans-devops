package codearchive;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Branch implements codearchive.BranchObservable {
    public final String name;
    public List<codearchive.BranchObserver> observers = new ArrayList<>();
    private LocalDateTime lastUpdated;
    private codearchive.Code code;

    public Branch(String name) {
        this.name = name;
        this.code = new codearchive.Code("");
        this.lastUpdated = LocalDateTime.now();
    }

    public codearchive.Code getCode() {
        return this.code;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void pushCommit(codearchive.Commit commit) {
        if (commit.createdOn.compareTo(lastUpdated) >= 0) {
            this.code = commit.newCode;
            this.notifyObservers();
            this.lastUpdated = LocalDateTime.now();
        }
    }

    @Override
    public void registerObserver(codearchive.BranchObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(codearchive.BranchObserver o) {
        observers.remove(o);

    }

    @Override
    public void notifyObservers() {
        for (var x : observers) {
            x.update();
        }
    }
}
