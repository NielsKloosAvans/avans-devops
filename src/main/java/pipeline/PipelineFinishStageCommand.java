package pipeline;

import java.util.ArrayList;

public class PipelineFinishStageCommand implements Command {
    public ArrayList<PipelineJob> pipelineJobs;

    public PipelineFinishStageCommand(ArrayList<PipelineJob> pipelineJobs) {
        this.pipelineJobs = pipelineJobs;
    }

    @Override
    public void execute() {
        for (var j : pipelineJobs) {
            j.prevStage = j.stage;
            j.finish(j.success);
        }
    }

    @Override
    public void undo() {
        for (var j : pipelineJobs) {
            j.stage = j.prevStage;
        }
    }
}
