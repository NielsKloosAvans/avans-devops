package pipeline;

public interface Command {
    void execute();
    void undo();
}
