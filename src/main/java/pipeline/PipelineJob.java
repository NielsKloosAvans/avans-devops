package pipeline;

public class PipelineJob {
    public static final int OFF = 0;
    public static final int QUEUED = 1;
    public static final int RUNNING = 2;
    public static final int FINISHED = 3;
    public static final int FAILED = 4;
    public int stage;
    public int prevStage;
    boolean success;

    public PipelineJob() {
        stage = OFF;
    }

    public void queue() {
        stage = QUEUED;
    }

    public void run() {
        stage = RUNNING;
        // very fancy commands
        success = true;
    }

    public void finish(boolean success) {
        if (success) {
            stage = FINISHED;
        } else {
            stage = FAILED;
        }
    }

    public void stop() {
        stage = FAILED;
    }
}
