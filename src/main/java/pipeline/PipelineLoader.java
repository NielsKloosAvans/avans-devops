package pipeline;

import codearchive.BranchObserver;

import java.util.ArrayList;

public class PipelineLoader implements BranchObserver {
    public StartPipelineJobMacroCommand startPipelineJobCommand;
    public StopPipelineJobMacroCommand stopPipelineJobCommand;

    public PipelineLoader(ArrayList<Command> commands) {
        startPipelineJobCommand = new StartPipelineJobMacroCommand(commands);
        stopPipelineJobCommand = new StopPipelineJobMacroCommand(commands);
    }

    @Override
    public void update() {
        startPipelineJobCommand.execute();
    }
}
