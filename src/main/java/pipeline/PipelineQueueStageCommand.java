package pipeline;

import java.util.ArrayList;

public class PipelineQueueStageCommand implements Command {
    public ArrayList<PipelineJob> pipelineJobs;

    public PipelineQueueStageCommand(ArrayList<PipelineJob> pipelineJobs) {
        this.pipelineJobs = pipelineJobs;
    }
    @Override
    public void execute() {
        for(var j : pipelineJobs) {
            j.prevStage = j.stage;
            j.queue();
        }
    }

    @Override
    public void undo() {
        for (var j : pipelineJobs) {
            j.stage = j.prevStage;
        }
    }
}
