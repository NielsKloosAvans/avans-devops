package pipeline;

import java.util.ArrayList;

public class PipelineRunStageCommand implements Command {
    public ArrayList<PipelineJob> pipelineJobs;
    public PipelineRunStageCommand(ArrayList<PipelineJob> pipelineJobs) {
        this.pipelineJobs = pipelineJobs;
    }
    @Override
    public void execute() {
        for(var j : pipelineJobs) {
            j.prevStage = j.stage;
            j.run();
        }
    }
    @Override
    public void undo() {
        for (var j : pipelineJobs) {
            j.stage = j.prevStage;
        }
    }
}
