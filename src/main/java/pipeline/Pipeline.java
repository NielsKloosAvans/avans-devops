package pipeline;

public class Pipeline {
    public Command[] onCommands = new Command[8];
    public Command[] offCommands = new Command[8];

    public void setCommand(int stage, Command onCommand, Command offCommand) {
        this.onCommands[stage] = onCommand;
        this.offCommands[stage] = offCommand;
    }


}
