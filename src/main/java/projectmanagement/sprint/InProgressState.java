package projectmanagement.sprint;

import java.time.LocalDateTime;

public class InProgressState extends  SprintState{
    public InProgressState(Sprint sprint) {
        super(sprint);
    }

    @Override
    public void update() {
        if (!sprint.getEndDate().isBefore(LocalDateTime.now().toLocalDate())) {
            this.sprint.setState(this.sprint.finishedState);
        }
    }
}
