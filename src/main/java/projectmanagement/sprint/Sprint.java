package projectmanagement.sprint;

import projectmanagement.backlogitem.BacklogItem;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class Sprint {
    public final String name;
    public final SprintState scheduledState;
    public final SprintState inProgressState;
    public final SprintState finishedState;
    private final LocalDate endDate;
    private final List<BacklogItem> backlogItems = new ArrayList<>();
    private LocalDate startDate;

    private SprintState sprintState;

    protected Sprint(LocalDateTime startDate) {

        this.scheduledState = new ScheduledState(this);
        this.inProgressState = new InProgressState(this);
        this.finishedState = new FinishedState(this);

        this.sprintState = scheduledState;

        var num_of_week = LocalDateTime.from(startDate).getDayOfWeek().getValue() - 1;
        var monday_in_week = LocalDateTime.from(startDate).minusDays(num_of_week).toLocalDate();
        this.startDate = monday_in_week;
        this.endDate = monday_in_week.plusDays(6);

        var ISOWeekOfMonth = WeekFields.ISO.weekOfMonth();
        var weekOfMonth = ISOWeekOfMonth.getFrom(startDate) - 1;

        this.name = (this.startDate.getMonth().toString() + " week " + weekOfMonth).toUpperCase(Locale.ROOT);
    }

    public void update() {
        this.sprintState.update();
    }

    public void addBacklogItem(BacklogItem backlogItem) {
        this.sprintState.addBacklogItem(backlogItem);
    }

    protected void addBacklogItemInState(BacklogItem backlogItem) {
        this.backlogItems.add(backlogItem);
    }

    protected void removeBacklogItemInState(BacklogItem backlogItem) {
        this.backlogItems.remove(backlogItem);
    }

    public void removeBacklogItem(BacklogItem backlogItem) {
        this.sprintState.removeBacklogItem(backlogItem);
    }

    public List<BacklogItem> getBacklogItems() {
        return this.backlogItems;
    }

    void setState(SprintState sprintState) {
        this.sprintState = sprintState;
    }

    public SprintState getState() {
        return this.sprintState;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return this.endDate;
    }
}
