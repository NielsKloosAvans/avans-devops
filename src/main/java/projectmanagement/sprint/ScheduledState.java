package projectmanagement.sprint;

import java.time.LocalDateTime;

public class ScheduledState extends  SprintState{
    public ScheduledState(Sprint sprint) {
        super(sprint);
    }

    @Override
    public void update() {
        if (!sprint.getStartDate().isAfter(LocalDateTime.now().toLocalDate())) {
            this.sprint.setState(this.sprint.inProgressState);
        }
    }

}
