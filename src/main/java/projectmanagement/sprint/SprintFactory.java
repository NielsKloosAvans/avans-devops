package projectmanagement.sprint;

import java.time.LocalDateTime;

public class SprintFactory {
    public Sprint getSprint(SprintType type, LocalDateTime startDate) {
        return switch (type) {
            case DEPLOYMENT -> new DeploymentSprint(startDate);
            case DEVELOPMENT -> new DevelopmentSprint(startDate);
        };
    }

    public enum SprintType {
        DEVELOPMENT,
        DEPLOYMENT
    }
}
