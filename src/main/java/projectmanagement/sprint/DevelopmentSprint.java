package projectmanagement.sprint;

import java.time.LocalDateTime;

public class DevelopmentSprint extends Sprint {
    public DevelopmentSprint(LocalDateTime startDate) {
        super(startDate);
    }
}
