package projectmanagement;

import java.util.List;

public class PremiumReplyManagementStrategy implements ReplyManagementStrategy {
    @Override
    public void addReply(List<Reply> replies, Reply r) {
        if (replies.size() < 10) {
            replies.add(r);
        } else {
            throw new RuntimeException("Cannot add reply - limit reached.");
        }
    }

    @Override
    public void removeReply(List<Reply> replies, Reply r) {
        replies.remove(r);
    }
}
