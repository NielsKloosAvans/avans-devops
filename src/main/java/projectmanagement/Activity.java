package projectmanagement;

public class Activity {
    private Developer developer;
    String name;
    String description;
    boolean isDone;

    public Activity(String name, String description) {
        this.name = name;
        this.description = description;
        isDone = false;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDone(boolean isDone) {
        this.isDone = isDone;
    }
    public boolean getDone() {
        return this.isDone;
    }
}
