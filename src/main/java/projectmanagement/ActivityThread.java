package projectmanagement;

import java.util.ArrayList;
import java.util.List;

public class ActivityThread {
    private final List<Reply> replies = new ArrayList<>();
    String content;
    Developer byDeveloper;

    private ReplyManagementStrategy replyManagementStrategy;

    public ActivityThread(String content, Developer byDeveloper, ReplyManagementStrategy replyManagementStrategy) {
        this.content = content;
        this.byDeveloper = byDeveloper;
        this.replyManagementStrategy = replyManagementStrategy;
    }

    public void addReply(Reply r) {
        replyManagementStrategy.addReply(replies, r);
    }

    public void removeReply(Reply r) {
        replyManagementStrategy.removeReply(replies, r);
    }

    public List<Reply> getReplies() {
        return replies;
    }
}
