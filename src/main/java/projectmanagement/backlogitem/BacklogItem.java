package projectmanagement.backlogitem;

import projectmanagement.Activity;
import projectmanagement.ActivityThread;
import projectmanagement.Developer;
import projectmanagement.Reply;
import projectmanagement.notificator.BacklogObservable;
import projectmanagement.notificator.BacklogObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class BacklogItem implements BacklogObservable {
    public Logger logger = Logger.getLogger(String.valueOf(this));
    public final BacklogItemState todoState;
    public final BacklogItemState doingState;
    public final BacklogItemState readyForTestingState;
    public final BacklogItemState testingState;
    public final BacklogItemState testedState;
    public final BacklogItemState doneState;
    boolean done;

    ArrayList<BacklogObserver> observers = new ArrayList<>();
    protected List<Activity> activities = new ArrayList<>();
    protected List<ActivityThread> activityThreads = new ArrayList<>();
    protected Developer developer;
    protected Developer tester;
    protected boolean completed;
    protected boolean testingDone;
    protected BacklogItemState backlogItemState;

    public BacklogItem() {
        this.todoState = new TodoState(this);
        this.doingState = new DoingState(this);
        this.readyForTestingState = new ReadyForTestingState(this);
        this.testingState = new TestingState(this);
        this.testedState = new TestedState(this);
        this.doneState = new DoneState(this);

        this.backlogItemState = this.todoState;
    }

    public BacklogItemState getState() {
        return backlogItemState;
    }

    @Override
    public void registerObserver(BacklogObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(BacklogObserver o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (var o : observers) {
            o.update(this.backlogItemState);
        }
    }

    public void assignDeveloper(Developer developer) {
        this.backlogItemState.assignDeveloper(developer);
    }

    public void assignDeveloperToActivity(Developer developer, Activity activity) {
        this.backlogItemState.assignDeveloperToActivity(developer, activity);
    }

    public Developer getDeveloper() {
        if (!activities.isEmpty()) {
            return null;
        } else {
            return this.developer;
        }
    }

    public Developer getTester() {
        return this.tester;
    }

    public void addActivity(Activity activity) {
        this.backlogItemState.addActivity(activity);
    }

    public void removeActivity(Activity activity) {
        this.backlogItemState.removeActivity(activity);
    }

    public void addActivityThread(ActivityThread activityThread) {
        this.backlogItemState.addActivityThread(activityThread);
    }

    public void removeActivityThread(ActivityThread activityThread) {
        this.backlogItemState.removeActivityThread(activityThread);
    }

    public void addReplyToActivityThread(int activityThread, Reply reply) {
        this.backlogItemState.addReplyToActivityThread(activityThread, reply);
    }

    public void removeReplyToActivityThread(int activityThread, Reply reply) {
        this.backlogItemState.removeReplyToActivityThread(activityThread, reply);
    }

    public void setCompleted(boolean completed) {
        this.backlogItemState.setCompleted(completed);
    }

    public void setTestSuccessful(boolean tested) {
        this.backlogItemState.setTestSuccessful(tested);
    }

    public void assignTester(Developer tester) {
        this.backlogItemState.assignTester(tester);
    }

    public void setState(BacklogItemState backlogItemState) {
        this.backlogItemState = backlogItemState;
        if (this.backlogItemState == this.readyForTestingState) {
            logger.info("Item is ready for testing, notify testers!");
            notifyObservers();
        } else if (this.backlogItemState == this.doneState) {
            logger.info("Item finished!");
        }
    }

    public void setDone(Developer developer, boolean done) {
        this.backlogItemState.setDone(developer, done);
    }

    public boolean isDone() {
        return this.done;
    }

    public boolean isTestingDone() {
        return this.testingDone;
    }

    public List<Activity> getActivities() {
        return this.activities;
    }

    public List<ActivityThread> getActivityThreads() {
        return this.activityThreads;
    }
}
