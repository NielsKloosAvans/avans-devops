package projectmanagement.backlogitem;

import projectmanagement.Activity;

public class DoingState extends BacklogItemState {
    @Override
    void addActivity(Activity activity) {
        this.backlogItem.activities.add(activity);
    }

    @Override
    void removeActivity(Activity activity) {
        this.backlogItem.activities.remove(activity);
    }

    public DoingState(BacklogItem backlogItem) {
        super(backlogItem);
    }

    @Override
    public void setCompleted(boolean completed) {
        this.backlogItem.completed = completed;
        if (this.backlogItem.completed) {
            if (this.backlogItem.activities.isEmpty()) {
                this.backlogItem.setState(this.backlogItem.readyForTestingState);
            } else {
                var completeTemp = true;
                for (var x : this.backlogItem.activities) {
                    if (!x.getDone()) {
                        completeTemp = false;
                        break;
                    }
                }

                if (completeTemp) {
                    this.backlogItem.setState(this.backlogItem.readyForTestingState);
                }
            }
        }

    }

}
