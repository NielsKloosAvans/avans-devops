package projectmanagement.backlogitem;

import projectmanagement.Developer;

public class ReadyForTestingState extends BacklogItemState {

    public ReadyForTestingState(BacklogItem backlogItem) {
        super(backlogItem);
    }

    @Override
    void assignTester(Developer tester) {
        if (tester.isTester) {
            this.backlogItem.tester = tester;
            this.backlogItem.setState(this.backlogItem.testingState);
        }
    }
}
