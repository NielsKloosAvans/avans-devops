package projectmanagement.backlogitem;

import projectmanagement.Activity;
import projectmanagement.ActivityThread;
import projectmanagement.Developer;
import projectmanagement.Reply;

public abstract class BacklogItemState {
    BacklogItem backlogItem;

    protected BacklogItemState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    void addActivity(Activity activity) {
    }

    void removeActivity(Activity activity) {
    }

    void addActivityThread(ActivityThread activityThread) {
        this.backlogItem.activityThreads.add(activityThread);
    }

    void removeActivityThread(ActivityThread activityThread) {
        this.backlogItem.activityThreads.remove(activityThread);
    }
    void addReplyToActivityThread(int activityThreadIndex, Reply reply) {
        var thread = this.backlogItem.activityThreads.get(activityThreadIndex);
        thread.addReply(reply);
    }

    void removeReplyToActivityThread(int activityThreadIndex, Reply reply) {
        var thread = this.backlogItem.activityThreads.get(activityThreadIndex);
        thread.removeReply(reply);
    }

     void assignDeveloper(Developer developer) {
    }

    void assignDeveloperToActivity(Developer developer, Activity activity) {
        activity.setDeveloper(developer);
    }

    void setCompleted(boolean completed) {
    }

    void setTestSuccessful(boolean tested) {
    }

    void assignTester(Developer tester) {
    }

    void setDone(Developer leadDeveloper, boolean done) {
    }

}
