package projectmanagement.backlogitem;

public class TestingState extends BacklogItemState {
    public TestingState(BacklogItem backlogItem) {
        super(backlogItem);
    }

    @Override
    public void setTestSuccessful(boolean tested) {
        this.backlogItem.testingDone = tested;
        if (this.backlogItem.testingDone) {
            this.backlogItem.setState(this.backlogItem.testedState);
        } else {
            this.backlogItem.setState(this.backlogItem.todoState);
        }

    }
}
