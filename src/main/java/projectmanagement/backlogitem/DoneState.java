package projectmanagement.backlogitem;

import projectmanagement.ActivityThread;
import projectmanagement.Reply;

public class DoneState extends BacklogItemState {
    public DoneState(BacklogItem backlogItem) {
        super(backlogItem);
    }

    @Override
    void addActivityThread(ActivityThread activityThread) {
        // backlog item is done and archived, can't edit
    }

    @Override
    void removeActivityThread(ActivityThread activityThread) {
        // backlog item is done and archived, can't edit
    }

    @Override
    void addReplyToActivityThread(int activityThreadIndex, Reply reply) {
        // backlog item is done and archived, can't edit
    }

    @Override
    void removeReplyToActivityThread(int activityThreadIndex, Reply reply) {
        // backlog item is done and archived, can't edit
    }
}
