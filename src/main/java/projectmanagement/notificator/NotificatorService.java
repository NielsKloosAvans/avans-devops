package projectmanagement.notificator;

public interface NotificatorService {
    void sendMessage();
}
