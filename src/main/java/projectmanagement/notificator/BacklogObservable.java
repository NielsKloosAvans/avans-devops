package projectmanagement.notificator;


public interface BacklogObservable {
    void registerObserver(BacklogObserver o);
    void removeObserver(BacklogObserver o);

    void notifyObservers();
}
