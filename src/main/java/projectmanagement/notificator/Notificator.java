package projectmanagement.notificator;

import projectmanagement.Developer;
import projectmanagement.backlogitem.BacklogItemState;

public class Notificator implements BacklogObserver {
    private BacklogItemState backlogItemState;
    public Developer developer;

    public Notificator(BacklogItemState bs, Developer developer) {
        this.backlogItemState = bs;
        this.developer = developer;
    }

    public BacklogItemState getState() {
        return this.backlogItemState;
    }

    @Override
    public void update(BacklogItemState bs) {
        if (developer.isTester) {
            this.developer.preferredService.sendMessage();
        }
        this.backlogItemState = bs;
    }
}
