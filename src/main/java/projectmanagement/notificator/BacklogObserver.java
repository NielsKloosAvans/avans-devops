package projectmanagement.notificator;

import projectmanagement.backlogitem.BacklogItemState;

public interface BacklogObserver {
    void update(BacklogItemState bs);
}
