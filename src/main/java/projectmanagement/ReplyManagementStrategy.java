package projectmanagement;

import java.util.List;

public interface ReplyManagementStrategy {
    void addReply(List<Reply> replies, Reply r);
    void removeReply(List<Reply> replies, Reply r);
}

