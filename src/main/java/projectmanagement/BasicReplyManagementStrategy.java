package projectmanagement;

import java.util.List;

public class BasicReplyManagementStrategy implements ReplyManagementStrategy {
    @Override
    public void addReply(List<Reply> replies, Reply r) {
        replies.add(r);
    }

    @Override
    public void removeReply(List<Reply> replies, Reply r) {
        replies.remove(r);
    }
}