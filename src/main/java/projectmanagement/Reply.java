package projectmanagement;

public class Reply {
    String content;
    Developer byDeveloper;

    public Reply(String content, Developer byDeveloper) {
        this.content = content;
        this.byDeveloper = byDeveloper;
    }
}
