package projectmanagement;

import projectmanagement.sprint.Sprint;
import projectmanagement.sprint.SprintFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Backlog {
    SprintFactory sprintFactory = new SprintFactory();
    List<Sprint> sprints = new ArrayList<>();

    public void addSprint(SprintFactory.SprintType sprintType, LocalDateTime startDate) {
        var sprint = sprintFactory.getSprint(sprintType, startDate);
        var contains = false;
        for (var i = 0; i < sprints.size(); i++) {
            var s = sprints.get(i);
            if (sprint.getStartDate().compareTo(s.getStartDate()) < 0.01) {
                contains = true;
                sprints.set(i, sprint);
                break;
            }
        }
        if (!contains) {
            this.sprints.add(sprint);
        }
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void removeSprint(Sprint sprint) {
        this.sprints.remove(sprint);
    }

}
