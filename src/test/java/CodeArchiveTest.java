import codearchive.Branch;
import codearchive.Code;
import codearchive.Commit;
import codearchive.Repository;
import org.junit.Assert;
import org.junit.Test;
import projectmanagement.Developer;
import projectmanagement.notificator.WhatsappAdapter;

public class CodeArchiveTest {

    @Test
    public void branch_should_use_name() {
        var branch = new Branch("Test");

        Assert.assertEquals("Test", branch.name);
        Assert.assertEquals("", branch.getCode().getContent());
    }

    @Test
    public void repository_should_be_created_with_main_branch() {
        var repository = new Repository("main");
        Assert.assertNotNull(repository.mainBranch);
        Assert.assertEquals("main", repository.mainBranch.name);
        Assert.assertNotNull(repository.getMainCode());
    }

    @Test
    public void commit_should_not_update_branch_when_date_is_earlier() {
        var developer = new Developer("developer", false, false, new WhatsappAdapter());
        var code = new Code("Some new fancy code");

        var branch = new Branch("main");

        var commit1 = new Commit(developer, code);
        var commit2 = new Commit(developer, code);
        var commit3 = new Commit(developer, code);

        commit1.createdOn = branch.getLastUpdated().minusSeconds(1); // made a day after branch was created
        commit2.createdOn = branch.getLastUpdated().plusSeconds(1); // made a day after branch was created
        commit3.createdOn = branch.getLastUpdated().plusDays(1); // made a day after branch was created

        branch.pushCommit(commit1);
        Assert.assertNotEquals(commit1.newCode, branch.getCode());
        Assert.assertNotEquals(commit1.newCode.getContent(), branch.getCode().getContent());

        branch.pushCommit(commit2);
        Assert.assertEquals(commit2.newCode, branch.getCode());
        Assert.assertEquals(commit2.newCode.getContent(), branch.getCode().getContent());

        branch.pushCommit(commit3);
        Assert.assertEquals(commit3.newCode, branch.getCode());
        Assert.assertEquals(commit3.newCode.getContent(), branch.getCode().getContent());
    }
}
