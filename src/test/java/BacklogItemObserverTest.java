import org.junit.Assert;
import org.junit.Test;
import projectmanagement.Developer;
import projectmanagement.backlogitem.BacklogItem;
import projectmanagement.notificator.Notificator;
import projectmanagement.notificator.WhatsappAdapter;

public class BacklogItemObserverTest {
    @Test
    public void notificator_should_get_state_from_backlogitem() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        var observer = new Notificator(backlogItem.getState(), developer);
        backlogItem.registerObserver(observer);

        Assert.assertEquals(observer.getState(), backlogItem.todoState);
    }

    @Test
    public void notificator_should_get_updated_state_when_state_is_updated() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        var observer = new Notificator(backlogItem.getState(), developer);
        backlogItem.registerObserver(observer);

        Assert.assertEquals(observer.getState(), backlogItem.todoState);

        backlogItem.setState(backlogItem.readyForTestingState);

        Assert.assertNotEquals(observer.getState(), backlogItem.todoState);
        Assert.assertEquals(observer.getState(), backlogItem.readyForTestingState);
    }

    @Test
    public void notificator_should_not_get_updated_when_state_is_not_ready_for_testing_state() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        var observer = new Notificator(backlogItem.getState(), developer);
        backlogItem.registerObserver(observer);

        Assert.assertEquals(observer.getState(), backlogItem.todoState);

        backlogItem.setState(backlogItem.doingState);

        Assert.assertEquals(observer.getState(), backlogItem.todoState);
        Assert.assertNotEquals(observer.getState(), backlogItem.readyForTestingState);
    }
}
