import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import projectmanagement.*;
import projectmanagement.notificator.WhatsappAdapter;

public class ActivityThreadTest {

    private Developer developer;
    private ActivityThread thread;
    private Reply reply1, reply2, reply3, reply4, reply5, reply6, reply7, reply8, reply9, reply10;

    @Before
    public void setUp() {
        developer = new Developer("John Doe", false, true, new WhatsappAdapter());
        thread = new ActivityThread("Test Thread", developer, new BasicReplyManagementStrategy());
        reply1 = new Reply("Test Reply 1", developer);
        reply2 = new Reply("Test Reply 2", developer);
        reply3 = new Reply("Test Reply 3", developer);
        reply4 = new Reply("Test Reply 4", developer);
        reply5 = new Reply("Test Reply 5", developer);
        reply6 = new Reply("Test Reply 6", developer);
        reply7 = new Reply("Test Reply 7", developer);
        reply8 = new Reply("Test Reply 8", developer);
        reply9 = new Reply("Test Reply 9", developer);
        reply10 = new Reply("Test Reply 10", developer);
    }

    @Test
    public void testAddReply() {
        thread.addReply(reply1);
        thread.addReply(reply2);
        thread.addReply(reply3);

        assertEquals(3, thread.getReplies().size());
    }

    @Test
    public void testRemoveReply() {
        thread.addReply(reply1);
        thread.addReply(reply2);
        thread.addReply(reply3);

        thread.removeReply(reply2);

        assertEquals(2, thread.getReplies().size());
        assertFalse(thread.getReplies().contains(reply2));
    }
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    @Test
    public void testPremiumReplyLimit() {
        thread = new ActivityThread("Test Thread", developer, new PremiumReplyManagementStrategy());
        thread.addReply(reply1);
        thread.addReply(reply2);
        thread.addReply(reply3);
        thread.addReply(reply4);
        thread.addReply(reply5);
        thread.addReply(reply6);
        thread.addReply(reply7);
        thread.addReply(reply8);
        thread.addReply(reply9);
        thread.addReply(reply10);
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Cannot add reply - limit reached.");
        thread.addReply(reply10);
    }

    @Test
    public void testPremiumRemove(){
        thread = new ActivityThread("Test Thread", developer, new PremiumReplyManagementStrategy());
        thread.addReply(reply1);
        thread.addReply(reply2);
        thread.addReply(reply3);
        thread.removeReply(reply2);
        assertEquals(2,thread.getReplies().size());
        assertTrue(thread.getReplies().contains(reply1));
        assertFalse(thread.getReplies().contains(reply2));
        assertTrue(thread.getReplies().contains(reply3));
    }

}