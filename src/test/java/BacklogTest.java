import org.junit.Assert;
import org.junit.Test;
import projectmanagement.Backlog;
import projectmanagement.sprint.DeploymentSprint;
import projectmanagement.sprint.DevelopmentSprint;
import projectmanagement.sprint.SprintFactory;

import java.time.LocalDateTime;

public class BacklogTest {
    @Test
    public void backlog_should_add_sprint_from_type() {
        var backlog = new Backlog();
        backlog.addSprint(SprintFactory.SprintType.DEPLOYMENT, LocalDateTime.of(2022, 3, 2, 0, 0, 0));

        Assert.assertEquals(1, backlog.getSprints().size());
    }

    @Test
    public void backlog_shouldnt_add_sprint_on_same_date() {
        var backlog = new Backlog();
        backlog.addSprint(SprintFactory.SprintType.DEPLOYMENT, LocalDateTime.of(2022, 3, 2, 0, 0, 0));
        backlog.addSprint(SprintFactory.SprintType.DEPLOYMENT, LocalDateTime.of(2022, 3, 2, 0, 0, 0));

        Assert.assertEquals(1, backlog.getSprints().size());
    }

    @Test
    public void backlog_should_update_sprint_when_on_same_date() {
        var backlog = new Backlog();
        var date = LocalDateTime.of(2022, 3, 2, 0, 0, 0);

        backlog.addSprint(SprintFactory.SprintType.DEVELOPMENT, date);
        Assert.assertEquals(DevelopmentSprint.class, backlog.getSprints().get(0).getClass());
        backlog.addSprint(SprintFactory.SprintType.DEPLOYMENT, date);
        Assert.assertEquals(DeploymentSprint.class, backlog.getSprints().get(0).getClass());
    }
}
