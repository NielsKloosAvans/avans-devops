import codearchive.Code;
import codearchive.Commit;
import codearchive.Repository;
import org.junit.Assert;
import org.junit.Test;
import pipeline.Command;
import pipeline.PipelineJob;
import pipeline.PipelineLoader;
import pipeline.PipelineQueueStageCommand;
import projectmanagement.Developer;
import projectmanagement.notificator.WhatsappAdapter;

import java.util.ArrayList;

public class RepositoryTest {

    @Test
    public void commit_to_branch_should_invoke_start_pipeline_macro() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        var job = new PipelineJob();
        pipelineJobs.add(job);
        var commands = new ArrayList<Command>();
        var pipelineStage = new PipelineQueueStageCommand(pipelineJobs);
        commands.add(pipelineStage);

        var pipelineLoader = new PipelineLoader(commands);
        var repo = new Repository("main");
        repo.mainBranch.registerObserver(pipelineLoader);

        Assert.assertEquals(PipelineJob.OFF, job.stage);

        var commit = new Commit(new Developer("John", false, false, new WhatsappAdapter()), new Code(""));
        repo.mainBranch.pushCommit(commit);

        Assert.assertEquals(PipelineJob.QUEUED, job.stage);
    }
}
